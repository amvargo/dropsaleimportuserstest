<?php

namespace App\Services;
use App\Jobs\ImportUsers;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Http;
use App\Events\ImportUserUpdate;
class UserService
{
    /**
     * Импортировать пользователей из внешнего API и передать их на обработку в фоновую очередь.
     *
     * @return \Illuminate\Foundation\Bus\PendingDispatch  Результат диспетчера задачи.
     */
    public function importUsers()
    {
        try {
            $response = Http::get("https://randomuser.me/api/?results=5000");
            $data = $response->json()['results'];
        }catch (\Exception $e){
            $this->notifyUserUpdate('error');
        }
        $userService = new UserService();
        return (new UserRepository($userService))->importUsers($data);
    }

    /**
     * Объединяет поля дочерних массивов во вложенном массиве данных и преобразует их в один уровень.
     *
     * @param array $data Входные данные, содержащие вложенные массивы.
     * @return array Обработанный массив данных с объединенными полями.
     */
    public function mergeChildFields($data)
    {
        $flattenedArray = ['name_first', 'name_last', 'dob_age', 'email'];
        $result = array_map(function ($item) use ($flattenedArray) {
            $flattenedItem = $item;
            foreach ($item as $key => $value) {
                unset($flattenedItem[$key]);
                if (is_array($value)) {
                    foreach ($value as $subKey => $subValue) {
                        if (in_array("{$key}_{$subKey}", $flattenedArray)) {
                            if ($key === 'name' && $subKey === 'first') {
                                $flattenedItem['first_name'] = $subValue;
                                $flattenedItem['password'] = 'password';
                            } elseif ($key === 'name' && $subKey === 'last') {
                                $flattenedItem['last_name'] = $subValue;
                            } elseif ($key === 'dob' && $subKey === 'age') {
                                $flattenedItem['age'] = $subValue;
                            } else {
                                $flattenedItem[$key . '_' . $subKey] = $subValue;
                            }
                        }
                    }
                }
                if (in_array($key, $flattenedArray)) {
                    $flattenedItem[$key] = $value;
                }
            }
            return $flattenedItem;
        }, $data);

        return $this->exeptDuplicate($result);
    }

    /**
     * Отправить в вебсокет уведомление о добавлении или обновлении пользователя
     *
     * @param integer $updatedUsers Обновлено пользователей.
     * @param integer $addedUsers Добавлено пользователей.
     * @param integer $totalUsers Общее количество пользователей.
     * @param integer $countData Количество добавляемых пользователй.
     */
    public function notifyUserUpdate($type, $updatedUsers = 0, $addedUsers = 0, $totalUsers = 0, $countData = 0)
    {
        ImportUserUpdate::dispatch([
            'type' => $type,
            'updated' => $updatedUsers,
            'added' => $addedUsers,
            'total' => $totalUsers,
            'percent' => ($addedUsers+$updatedUsers > 0) ? round((($addedUsers + $updatedUsers) / $countData) * 100, 2) : 0,
        ]);
    }

    /**
     * Удаляет дубликаты из массива данных, используя комбинацию имени и фамилии в качестве уникального идентификатора.
     *
     * @param array $data Массив данных, в котором необходимо удалить дубликаты.
     * @return array Массив данных без дубликатов.
     */
    public function exeptDuplicate($data){
        $data = array_reduce($data, function ($carry, $item) {
            $id = "{$item['first_name']} {$item['last_name']}";

            if (!isset($carry[$id])) {
                $carry[$id] = $item;
            } else {
                $id = "{$item['first_name']}ing {$item['last_name']}ing";
                $carry[$id] = $item;
                $carry[$id]['first_name'] = "{$item['first_name']}ing";
                $carry[$id]['last_name'] = "{$item['last_name']}ing";
            }

            return $carry;
        }, []);

        return $data;
    }
}
