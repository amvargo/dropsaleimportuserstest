<?php

namespace App\Repositories;

use App\Events\ImportUserUpdate;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Services\UserService;
class UserRepository
{
    /**
     * Создаем новый экземпляр класса и внедряет зависимость UserService.
     *
     * @param UserService $userService Зависимость UserService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Импортировать пользователей из API и обновить базу данных.
     *
     * @param array $data Данные о пользователях из API.
     * @return void
     */
    public function importUsers($data)
    {
        $originalRowCount = $this->getCountAllUsers();
        $addedUsers = 0;
        $updatedUsers = 0;
        $totalUsers = 0;

        DB::beginTransaction();

        try {
            $data = $this->userService->mergeChildFields($data);
            $chunks = array_chunk($data, 20);
            foreach ($chunks as $chunk) {
                $this->findOrCreateUser($chunk);
                $currentRowCount = $this->getCountAllUsers();
                $addedUsers = $currentRowCount-$originalRowCount;
                $totalUsers = $originalRowCount + $addedUsers;
                $updatedUsers = count($data) - $addedUsers; 
                if($addedUsers || $updatedUsers) {
                    $this->userService->notifyUserUpdate('update', $updatedUsers, $addedUsers, $totalUsers, count($data));
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Найти существующего пользователя или создать нового на основе данных из API.
     *
     * @param array $userData Данные пользователя из API.
     * @return User             Существующий или новый пользователь.
     */
    private function findOrCreateUser($userData)
    {
        try {
            return User::upsert($userData, ['first_name', 'last_name'], ['age', 'email', 'password']);
        } catch (\Exception $e) {
            $this->userService->notifyUserUpdate('error');
            DB::rollBack();
        }
    }

    /**
     * Получает и возвращает общее количество пользователей в базе данных.
     *
     * @return int Количество всех пользователей в базе данных.
     */
    private function getCountAllUsers() {
        return User::count();
    }
}
