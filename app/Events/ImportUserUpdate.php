<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;


class ImportUserUpdate implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;

    /**
     * Создать новый экземпляр события обновления импорта пользователей.
     *
     * @param  array  $data  Данные для передачи в событии.
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Определить имя события, какое будет использоваться при передаче.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'import.users.update';
    }

    /**
     * Каналы, на которых событие должно быть передано.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('notification');
    }

    /**
     * Определить данные, которые будут переданы с событием.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['data' => $this->data];
    }
}
