<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Services\UserService;
use App\Jobs\ImportUsers;

class UserController extends Controller
{
    private $userService;

    /**
     * Создать новый экземпляр контроллера.
     *
     * @param  UserService  $userService  Сервис пользователей.
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Отобразить главную страницу.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Index');
    }

    /**
     * Импортировать пользователей из внешнего API.
     *
     * @param  Request  $request  Запрос, содержащий параметры импорта.
     * @return \Inertia\Response
     */
    public function import(Request $request)
    {
        ImportUsers::dispatch($request->count);

        return Inertia::render('Index');
    }
}
